import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PodcastsComponent } from './components/podcasts/podcasts.component';
import { PodcastsPage } from './pages/podcasts/podcasts.page';
import { PodcastGenresPage } from './pages/podcast-genres/podcast-genres.page';

@NgModule({
  // all components need to be declared here
  declarations: [
    AppComponent,
    PodcastsComponent,
    PodcastsPage,
    PodcastGenresPage
  ],
  imports: [
    // all Angular Modules (NgModules) need to be imported here
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  // AppComponent is the root component
  bootstrap: [AppComponent]
})
export class AppModule { }
