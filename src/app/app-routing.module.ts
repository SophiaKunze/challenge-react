import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PodcastGenresPage } from './pages/podcast-genres/podcast-genres.page';
import { PodcastsPage } from './pages/podcasts/podcasts.page';

const routes: Routes = [
  {
    path: 'podcasts',
    component: PodcastsPage,
  },
  {
    path: 'podcasts-genres',
    component: PodcastGenresPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
