import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PodcastGenresPage } from './podcast-genres.page';

describe('PodcastGenresPage', () => {
  let component: PodcastGenresPage;
  let fixture: ComponentFixture<PodcastGenresPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PodcastGenresPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastGenresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
