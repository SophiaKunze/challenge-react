import { Component, OnInit } from '@angular/core';
import { Podcast } from 'src/app/models/podcast.model';

@Component({
  selector: 'app-podcasts-page',
  templateUrl: './podcasts.page.html',
  styleUrls: ['./podcasts.page.scss'],
})

export class PodcastsPage implements OnInit {
  podcasts: Podcast[] = [
    {
      title: 'are we bored yet',
      author: 'blue bears',
      image: 'assets/are-we-bored-yet.png',
    },
    {
      title: 'coffee time',
      author: 'sleepy bear',
      image: 'assets/coffee-time.jpg',
    },
    {
      title: 'unveiled reality',
      author: 'i.t.',
      image: 'assets/unrelieved-reality.jpg',
    },
    {
      title: 'dog fathers',
      author: 'daddy dog',
      image: 'assets/dog-fathers.jpg',
    },
    {
      title: 'eating the good',
      author: 'mr. salad',
      image: 'assets/eating-the-good.jpg',
    },
    {
      title: 'fly high',
      author: 'birdy',
      image: 'assets/fly-high.jpg',
    },
    {
      title: 'frenchie press',
      author: 'french bulldog',
      image: 'assets/frenchie-press.jpg',
    },
    {
      title: 'health talk',
      author: 'mrs. salad',
      image: 'assets/health-talk.png',
    },
    {
      title: 'monster',
      author: 'cookie monster',
      image: 'assets/monster.jpg',
    },
    {
      title: 'tech me',
      author: 'nerdy nola',
      image: 'assets/tech-me.jpg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
