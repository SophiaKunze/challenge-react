import { Component, Input } from '@angular/core';
import { Podcast } from 'src/app/models/podcast.model';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.component.html',
  styleUrls: ['./podcasts.component.scss'],
})
export class PodcastsComponent { //CHILD of page
  // define inputs that are sent from parent (page)
  @Input()
  podcasts: Podcast[] = [];

  appName = "Sophia's Podcast Collection";
  appVersion = 3;
  podcastCategories = ['Comedy', 'News', 'Politics', 'History', 'Sciences'];
  pageAuthor = { name: 'Sophia', githubLink: 'https://github.com/SophiaKu', avatar: '🦔' };
  constructor() {}
}
