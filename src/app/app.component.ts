import { Component } from '@angular/core';
// this file contains all the logic of the component

// decorator for indication what files is supposed to do; 
// in this case it indicated that this is a components file
@Component({ 
  selector: 'app-root', // how you render this component
  templateUrl: './app.component.html', // the html, also possible as inline component via template: ``
  styleUrls: ['./app.component.scss'] // array of css files
})
export class AppComponent {
  title = 'ng-podcasts';
}
