import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// angular is based on several modules, each based on several components
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
